webpackJsonp([2,5],{

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__category_data_service__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__query__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_uikit__ = __webpack_require__(440);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_uikit___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_uikit__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_uikit_dist_js_uikit_icons__ = __webpack_require__(439);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_uikit_dist_js_uikit_icons___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_uikit_dist_js_uikit_icons__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





__WEBPACK_IMPORTED_MODULE_3_uikit___default.a.use(__WEBPACK_IMPORTED_MODULE_4_uikit_dist_js_uikit_icons___default.a);
var CategoryListComponent = (function () {
    function CategoryListComponent(categoryService) {
        var _this = this;
        this.categoryService = categoryService;
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"](); //aa
        this.userNav = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        //@Output() change = new EventEmitter();//aa
        this.isMobile = window.screen.width < 415;
        this.SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };
        this.service = categoryService;
        categoryService.userNav.subscribe(function (item) { return _this.handleUserNav(item); });
    }
    CategoryListComponent.prototype.handleUserNav = function (ev) {
        console.log('I heard it changed');
        console.log(this.query);
        if (this.query.parent != null)
            this.query.parent.show = true;
        this.query.choose = true;
        this.query.set = null;
        this.query.show = false;
        this.change.emit({ category: this.query.set, id: this.query.id });
    };
    CategoryListComponent.prototype.swipe = function (query, action) {
        if (action === void 0) { action = this.SWIPE_ACTION.RIGHT; }
        console.log('SWIPE');
        this.closeQuery(query);
    };
    CategoryListComponent.prototype.closeQuery = function (query) {
        if (query.parent != null)
            query.parent.show = true;
        query.choose = true;
        query.set = null;
        this.change.emit({ category: this.query.set, id: this.query.id });
    };
    CategoryListComponent.prototype.newHeading = function (ev) {
        console.log('New Heading');
        console.log(ev);
        console.log(this.service.htmlHeading);
        var el = document.getElementById('catSelector');
        //if(el && el.options) console.log(el.options[ el.selectedIndex ].text)
    };
    CategoryListComponent.prototype.htmlClick = function (ev) {
        console.log(ev);
        if (ev && ev.target) {
            var el = ev.target;
            if (el.className == 'linker') {
                console.log('Linker clicked');
                var css = el.dataset.csslink;
                var c = this.service.findCategory(css);
                console.log(c);
                if (c) {
                    this.query.choose = false; //close choices
                    c.idx = this.query.id;
                    this.setCategory(c);
                    //console.log('JQ:',$('.result').get(this.query.id))
                    if (this.query.id > 0) {
                        this.query.parent.show = false;
                    }
                    if (this.service.currentQuery && this.service.currentQuery.set) {
                        //this.service.currentQuery.set.editme=false
                    }
                    this.service.currentQuery = this.query;
                }
                else {
                    console.log('linker categroy not found');
                }
            }
        }
    };
    CategoryListComponent.prototype.itemClick = function (c, ev, idx) {
        //this.query.set=c;
        this.query.choose = false; //close choices
        c.idx = idx;
        this.setCategory(c);
        //console.log('JQ:',$('.result').get(this.query.id))
        if (this.query.id > 0) {
            this.query.parent.show = false;
        }
        if (this.service.currentQuery && this.service.currentQuery.set) {
            //this.service.currentQuery.set.editme=false
        }
        this.service.currentQuery = this.query;
    };
    CategoryListComponent.prototype.handleBtnKeyPress = function (c, event) {
        // Check to see if space or enter were pressed
        if (event.keyCode === 32 || event.keyCode === 13) {
            // Prevent the default action to stop scrolling when space is pressed
            event.preventDefault();
            this.query.choose = false; //close choices
            this.setCategory(c);
            //console.log('JQ:',$('.result').get(this.query.id))
            if (this.query.id > 0) {
                this.query.parent.show = false;
            }
            if (this.service.currentQuery && this.service.currentQuery.set) {
                //this.service.currentQuery.set.editme=false
            }
            this.service.currentQuery = this.query;
        }
    };
    CategoryListComponent.prototype.validate = function (c) {
        console.log('Validating...');
        this.editing = null;
    };
    CategoryListComponent.prototype.setCategory = function (c) {
        console.log('Click');
        //c.editme=false;
        this.query.set = c;
        this.change.emit({ category: this.query.set, id: this.query.id });
        //
    };
    CategoryListComponent.prototype.ngOnInit = function () {
        console.log("LIST RESULTS");
        console.log(this.query);
    };
    return CategoryListComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__query__["a" /* Query */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__query__["a" /* Query */]) === "function" && _a || Object)
], CategoryListComponent.prototype, "query", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], CategoryListComponent.prototype, "change", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], CategoryListComponent.prototype, "userNav", void 0);
CategoryListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'category-list',
        template: __webpack_require__(406)
        //styleUrls: ['../app.css']
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__category_data_service__["a" /* CategoryDataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__category_data_service__["a" /* CategoryDataService */]) === "function" && _b || Object])
], CategoryListComponent);

var _a, _b;
//# sourceMappingURL=category-list.component.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Category; });
var Category = (function () {
    function Category(values) {
        if (values === void 0) { values = {}; }
        this.icon = "";
        this.name = "Category";
        this.desc = "";
        this.editme = false;
        this.html = 'assets/html/sports.html';
        this.subcategories = [];
        this.forceWide = false;
        this.part = '';
        this.available = true;
        Object.assign(this, values);
    }
    return Category;
}());

//# sourceMappingURL=category.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Query; });
var Query = (function () {
    function Query(values) {
        if (values === void 0) { values = {}; }
        this.id = 0;
        this.results = []; //list to display
        this.choose = false;
        this.name = "Top Level";
        this.show = false;
        this.path = [];
        Object.assign(this, values);
    }
    Query.prototype.clear = function () {
        this.set = null;
        this.results = [];
        this.show = false;
    };
    return Query;
}());

//# sourceMappingURL=query.js.map

/***/ }),

/***/ 180:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 180;


/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(198);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__category_query__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__category_category_data_service__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular_2_local_storage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular_2_local_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular_2_local_storage__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = (function () {
    function AppComponent(categoryservice, localStorageService) {
        var _this = this;
        this.categoryservice = categoryservice;
        this.localStorageService = localStorageService;
        this.queries = [];
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        // constant for swipe action: left or right
        this.isMobile = window.screen.width < 415;
        this.isPortrait = window.innerHeight > window.innerWidth;
        this.showSplash = true;
        this.numbers = {};
        this.service = categoryservice;
        this.store = localStorageService;
        categoryservice.userNav.subscribe(function (x) { return _this.handleUserNav(x); });
        if (this.isMobile && this.showSplash)
            $('.darken').css({ opacity: 0 });
        this.getData();
        window['appGlobals'].scope = this;
    }
    AppComponent.prototype.resetView = function () {
    };
    AppComponent.prototype.dimBg = function () {
        $('.darken').css({ opacity: 1 });
    };
    AppComponent.prototype.spacer = function (s) {
        var o = "";
        for (var i = 0; i < s; i++) {
            o += "--";
        }
        return o;
    };
    AppComponent.prototype.handleUserNav = function (ev) {
        console.log('***********************APP: USER Nav');
        //Nav to Support Options
    };
    Object.defineProperty(AppComponent.prototype, "categoryData", {
        get: function () {
            return JSON.stringify(this.topCategory);
        },
        enumerable: true,
        configurable: true
    });
    AppComponent.prototype.closeQuery = function () {
        console.log('App.CloseQuery');
        var q = this.service.currentQuery;
        if (q.parent != null)
            q.parent.show = true;
        q.choose = true;
        q.set = null;
        //this.change.emit({category:this.service.currentQuery.set,id:this.service.currentQuery.id})
        this.updateQueries(this.service.currentQuery.set, this.service.currentQuery.id);
        this.service.currentQuery = q.parent;
        $("#scrollable").animate({ scrollTop: 0 }, "slow");
    };
    AppComponent.prototype.openingPage = function () {
        return (this.queries[0] && this.queries[0].choose) ? true : false;
    };
    AppComponent.prototype.goEdit = function () {
        console.log('EDIT MODE');
        console.log(this.service.dataHTML);
        this.service.buildHeadings();
        this.service.editMode = !this.service.editMode;
        console.log(this.service.editMode);
    };
    AppComponent.prototype.loadLocal = function () {
        if (this.store.get('cats')) {
            this.categoryservice.categories = this.topCategory = this.store.get('cats');
            console.log('LOADED');
        }
        else {
            console.log('Nothing saved yet.');
        }
    };
    AppComponent.prototype.saveLocal = function () {
        this.store.set('cats', this.categoryservice.categories);
        console.log('SAVED');
    };
    AppComponent.prototype.scrollToElement = function (element) {
        var el = document.getElementById(element);
        var yPos = el.getClientRects()[0].top;
        var yScroll = window.scrollY;
        var interval = setInterval(function () {
            yScroll -= 10;
            window.scroll(0, yScroll);
            if (el.getClientRects()[0].top >= 0) {
                window.clearInterval(interval);
            }
        }, 5);
    };
    AppComponent.prototype.handleUpdate = function (ev) {
        console.log('APP: Handle Update');
        var i = parseInt(ev.id);
        this.updateQueries(ev.category, i);
        this.service.emerg = this.numbers;
    };
    AppComponent.prototype.updateQueries = function (c, i) {
        if (i <= this.queries.length) {
            //Set the next query up if needed
            var qu = this.queries[i];
            if (qu.set != null) {
                console.log('Parent has been set');
                var q = this.queries[i + 1];
                q.results = c.subcategories;
                q.show = true;
                q.choose = true;
                q.set = null;
            }
            else {
                console.log('Parent not set');
                var q = this.queries[i + 1];
                q.results = [];
                q.choose = false;
                q.show = false;
                q.set = null;
            }
        }
        else {
        }
    };
    AppComponent.prototype.setQueries = function () {
        var a = ["Physical or Violent Incidents", "Assault Physical"];
    };
    AppComponent.prototype.getData = function () {
        var _this = this;
        console.log('Get Data');
        this.categoryservice.getData().subscribe(function (data) {
            _this.service.categories = _this.topCategory = data.categories;
            console.log('PARSE');
            _this.queries.push(new __WEBPACK_IMPORTED_MODULE_1__category_query__["a" /* Query */]({ id: 0, name: "Start", choose: true, set: null, show: true }));
            _this.queries.push(new __WEBPACK_IMPORTED_MODULE_1__category_query__["a" /* Query */]({ id: 1, name: "Now What?", parent: _this.queries[0], show: true }));
            _this.queries.push(new __WEBPACK_IMPORTED_MODULE_1__category_query__["a" /* Query */]({ id: 2, name: "How?", parent: _this.queries[1], show: true }));
            _this.queries.push(new __WEBPACK_IMPORTED_MODULE_1__category_query__["a" /* Query */]({ id: 3, name: "How?", parent: _this.queries[2], show: true }));
            console.log(_this.topCategory);
            _this.queries[0].results = _this.topCategory.subcategories;
            _this.queries[1].results = _this.topCategory.subcategories;
            _this.queries[2].results = _this.topCategory.subcategories;
            _this.queries[3].results = _this.topCategory.subcategories;
            window['appGlobals'].numbers = data.numbers;
            _this.service.emerg = data.numbers;
            console.log(window['appGlobals'].numbers);
        }, function (err) {
            console.log(err);
        });
    };
    AppComponent.prototype.process = function (d) {
        console.log('Process');
        console.log(d);
        this.service.emerg = d.numbers;
        console.log(this.service.emerg);
    };
    AppComponent.prototype.extNav = function (s) {
        console.log('Navigate ...' + s);
        var cat = this.service.findCategory(s);
    };
    AppComponent.prototype.ngOnInit = function () {
        // 'bank' is the name of the route parameter
        //this.editMode = this.route.snapshot.queryParams['edit'];
    };
    return AppComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], AppComponent.prototype, "change", void 0);
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(407),
        //styleUrls: ['./app.sass'],
        providers: [__WEBPACK_IMPORTED_MODULE_2__category_category_data_service__["a" /* CategoryDataService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__category_category_data_service__["a" /* CategoryDataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__category_category_data_service__["a" /* CategoryDataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3_angular_2_local_storage__["LocalStorageService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_angular_2_local_storage__["LocalStorageService"]) === "function" && _b || Object])
], AppComponent);

var _a, _b;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_utils_sanatize_pipe__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular_2_local_storage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular_2_local_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular_2_local_storage__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__category_category_list_component__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__category_category_data_service__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__category_category_item_component__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__utils_html_component__ = __webpack_require__(196);
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






//import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome'
//import { RouterModule, Routes } from '@angular/router';





var firebaseConfig = {
    apiKey: "AIzaSyDIYwEWChAcuJhQmmMME3TpWyXD7rGIOa4",
    authDomain: "monash-app-321.firebaseapp.com",
    databaseURL: "https://monash-app-321.firebaseio.com",
    projectId: "monash-app-321",
    storageBucket: "monash-app-321.appspot.com",
    messagingSenderId: "558273548220"
};
/*
const appRoutes: Routes = [
  { path: '/', component: SplashComponent },
  { path: '/menu',      component: AppComponent},
  { path: '',
    redirectTo: '/menu',
    pathMatch: 'full'
  },

];
*/
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_7__category_category_list_component__["a" /* CategoryListComponent */],
            __WEBPACK_IMPORTED_MODULE_9__category_category_item_component__["a" /* CategoryItemComponent */],
            __WEBPACK_IMPORTED_MODULE_4_app_utils_sanatize_pipe__["a" /* SanitizeHtmlPipe */],
            __WEBPACK_IMPORTED_MODULE_10__utils_html_component__["a" /* MyHtml */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* JsonpModule */],
            __WEBPACK_IMPORTED_MODULE_5_angular_2_local_storage__["LocalStorageModule"].withConfig({
                prefix: 'limps',
                storageType: 'localStorage'
            })
            //RouterModule.forRoot(appRoutes)
            //AngularFireModule.initializeApp(firebaseConfig)
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_8__category_category_data_service__["a" /* CategoryDataService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__category__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__category_data_service__ = __webpack_require__(46);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryItemComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CategoryItemComponent = (function () {
    function CategoryItemComponent(categoryService) {
        this.categoryService = categoryService;
    }
    return CategoryItemComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__category__["a" /* Category */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__category__["a" /* Category */]) === "function" && _a || Object)
], CategoryItemComponent.prototype, "category", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], CategoryItemComponent.prototype, "label", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], CategoryItemComponent.prototype, "value", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], CategoryItemComponent.prototype, "ti", void 0);
CategoryItemComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'category-item',
        template: "\n    <div class=\"ci-block\" [ngClass]=\"{disabled: !category.available, wideMenu: category.wideMenu, 'twoLines': category.twoLines}\" tabindex=\"{{ti}}\" >\n      <div class=\"edgeBlock\">\n        <i class=\"fa fa-chevron-right\"></i>\n      </div>\n      <img src=\"./assets/images/icons/sports/{{category.icon}}\" *ngIf=\"category.icon\" alt=\"Category icon\"/>\n      \n      <div class=\"ci-text\" [ngClass]=\"{withIcon: category.icon}\">{{category.name}}</div>\n      \n    </div>\n    \n  "
        //styleUrls: ['../app.css']
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__category_data_service__["a" /* CategoryDataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__category_data_service__["a" /* CategoryDataService */]) === "function" && _b || Object])
], CategoryItemComponent);

var _a, _b;
//# sourceMappingURL=category-item.component.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_cheerio__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_cheerio___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_cheerio__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_category_category_data_service__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_underscore__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__category_category_list_component__ = __webpack_require__(110);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyHtml; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyHtml = (function () {
    function MyHtml(http, service) {
        this._url = '';
        this._part = '';
        this.myVal = "";
        this._myHTML = '';
        this._reqAuth = false;
        this.self = this;
        this.userNav = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.partChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this._http = http;
        //winRef.nativeWindow['appGlobal'].test=this;
        this.service = service;
    }
    Object.defineProperty(MyHtml.prototype, "reqAuth", {
        get: function () { return this._reqAuth; },
        set: function (r) {
            if (this.service.userPass == this.service.groupPass) {
                console.log('User pass is correct anyway');
                this._reqAuth = false;
            }
            else {
                if (r == true && (this.service.userPass != this.service.groupPass)) {
                    console.log('User pass is incorrect');
                    this._reqAuth = true;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyHtml.prototype, "url", {
        get: function () { return this._url; },
        set: function (url) {
            this.service.currentHtmlComp = this;
            if (url == '') {
                this._url = null;
                this.myVal = '';
            }
            else {
                //prevent reloading the same pa
                console.log('LOAD FILE');
                this.loadFile(url);
                this._url = url;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyHtml.prototype, "part", {
        get: function () { return this._part; },
        set: function (part) {
            if (part == '') {
                this._part = null;
            }
            else {
                console.log('new part');
                this._part = part;
            }
        },
        enumerable: true,
        configurable: true
    });
    MyHtml.prototype.test = function () {
        console.log('HTML has requested a nav change');
        this.userNav.emit({});
    };
    MyHtml.prototype.ngOnChanges = function (changes) {
        if (changes['url']) {
            console.log('URL change');
            return;
        }
        if (changes['part']) {
            console.log('CSS PART change');
            this._part = changes['part'].currentValue;
            this.myVal = this.filterHtml(this._myHTML);
            //this.service.dataHTML=this._myHTML;
            //this.service.setHTMLSrc(this._myHTML);
        }
    };
    MyHtml.prototype.loadFile = function (f) {
        var _this = this;
        if (f == this._url) {
            console.log('HTML file is already loaded');
            return;
        }
        console.log('======================Load file ' + f);
        this.service.getFile(f).subscribe(function (html) { return _this._myHTML = html; }, function (error) { console.log('Subscribe error'); }, function () { return _this.myVal = _this.filterHtml(_this._myHTML); });
    };
    MyHtml.prototype.popHTML = function () {
        this.myVal = this.filterHtml(this._myHTML);
    };
    MyHtml.prototype.filterHtml = function (html) {
        console.log('Filter HTML', this._part);
        //console.log(html)
        var $ = __WEBPACK_IMPORTED_MODULE_2_cheerio__["load"](html);
        var r = $(this._part);
        this.service.htmlHeading = $(this._part + ' .heading').text();
        console.log(this.service.htmlHeading);
        this.service.setHTMLData(this._myHTML);
        this.service.htmlPartData = r.html();
        var numbers = window['appGlobals'].numbers;
        if (this._part == ".l1.sub.numbers") {
            //we need to process this a bit
            console.log('Insert numbers');
            console.log(window['appGlobals'].numbers);
            $("#mainNumber").prepend('<span>Main Number &nbsp;</span><i class="fa fa-phone-square" aria-hidden="true"></i> <a class="telelink" href="tel:' + numbers.main + '"><span> ' + numbers.main + '</span></a>');
        }
        if (this._part == ".l1.sub.otherNumbers") {
            __WEBPACK_IMPORTED_MODULE_4_underscore__["each"](numbers.other, function (e, i) {
                $('#others').append('<li><span>' + numbers.other[i].label + '&nbsp;</span><i class="fa fa-phone-square" aria-hidden="true"></i> <a class="telelink" href="tel:' + numbers.other[i].ph + '"><span> ' + numbers.other[i].ph + '</span></a></li>');
                console.log($('#others').html());
            });
        }
        $(this._part + " > ul > li").each(function (i, e) {
            $(this).prepend("<div class='bullet'><div class='bulletIdx'>" + (i + 1) + "</div></div>");
        });
        this.partChange.emit({ heading: "test" });
        return r.html();
    };
    return MyHtml;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__category_category_list_component__["a" /* CategoryListComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__category_category_list_component__["a" /* CategoryListComponent */]) === "function" && _a || Object)
], MyHtml.prototype, "list", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], MyHtml.prototype, "reqAuth", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], MyHtml.prototype, "userNav", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
    __metadata("design:type", Object)
], MyHtml.prototype, "partChange", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [String])
], MyHtml.prototype, "url", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [String])
], MyHtml.prototype, "part", null);
MyHtml = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'my-html',
        template: "<div class=\"contentData\" [innerHTML]=\"myVal | sanitizeHtml\" *ngIf=\"_reqAuth==false\" ></div>\n  <div *ngIf=\"_reqAuth==true\" id=\"loginPanel\">\n    <label>You are required to login to access this information.</label>\n    <input [(ngModel)]=\"service.userPass\" (keyup)=\"_reqAuth = !service.validateUser()\"/>\n  </div>",
        //styleUrls: ['./../app.sass'],
        encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
        providers: [__WEBPACK_IMPORTED_MODULE_3_app_category_category_data_service__["a" /* CategoryDataService */]]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_app_category_category_data_service__["a" /* CategoryDataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_app_category_category_data_service__["a" /* CategoryDataService */]) === "function" && _c || Object])
], MyHtml);

var _a, _b, _c;
//# sourceMappingURL=html.component.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(45);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SanitizeHtmlPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SanitizeHtmlPipe = (function () {
    function SanitizeHtmlPipe(_sanitizer) {
        this._sanitizer = _sanitizer;
    }
    SanitizeHtmlPipe.prototype.transform = function (v) {
        return this._sanitizer.bypassSecurityTrustHtml(v);
    };
    return SanitizeHtmlPipe;
}());
SanitizeHtmlPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'sanitizeHtml'
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]) === "function" && _a || Object])
], SanitizeHtmlPipe);

var _a;
//# sourceMappingURL=sanatize-pipe.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 406:
/***/ (function(module, exports) {

module.exports = "<br *ngIf=\"query.set || isMobile==false\"/><div class=\"pos-rel theme-2\"><a [attr.id]=\"'panelTop'+query.id\"></a><div *ngIf=\"query.choose==true\" aria-label=\"Category list\"><div class=\"animated\" [ngClass]=\"{'slideInUp': isMobile==true, 'slideInDown': isMobile==false &amp;&amp; query.id==0, 'fadeIn delay-1': isMobile==false &amp;&amp; query.id&gt;0} \"><div *ngFor=\"let c of query.results; let i = index\"><category-item role=\"button\" aria-label=\"c.name\" [category]=\"c\" (keypress)=\"handleBtnKeyPress(c,$event)\" (click)=\"itemClick(c,$event,i)\" [ngClass]=\"{'selected': c==query.set} \" [ti]=\"query.index+ i+10\"></category-item></div></div></div><!--.grid-100.uk-align-left.closeBtn('(click)'=\"closeQuery(query)\" \"*ngIf\"=\"query.set!=null\")\n  i.fa.fa-chevron-left\n  | &nbsp;Back\n--><div class=\"animated\" *ngIf=\"query.set\" [ngClass]=\"{'fadeIn': isMobile==true} \"><div class=\"result panel uk-card uk-card-default uk-box-shadow-medium selectMode\" (click)=\"closeQuery(query)\"><div class=\"grid-100\" *ngIf=\"!isMobile\"></div><div class=\"grid-100 catBar padding-side-0 rowMode\"><a class=\"uk-align-right closeBtn-embedded-cat\" (click)=\"closeQuery(query)\" *ngIf=\"query.set!=null\" role=\"button\" aria-label=\"Back to menu\" tabindex=\"0\" href=\"javascript:void(0)\"><i class=\"fa fa-chevron-left\"></i></a><div class=\"grid-10 mobile-grid-10 padding-side-0\"><img class=\"icon\" src=\"assets/images/icons/sports/{{query.set.icon}}\" *ngIf=\"query.set.icon\" alt=\"Category icon\"/></div><div class=\"grid-90 mobile-grid-90 white\"><div class=\"bar\" style=\"font-size:16px;color:#ffffff;padding:6px;width:100%;\" [innerHtml]=\"query.parent.set.name\" *ngIf=\"query.parent!=null\"></div><div class=\"bar\" style=\"font-size:18px;font-weight:300;margin-left:6px;\" [innerHtml]=\"query.set.name\"></div></div></div><div class=\"clear\"></div></div><div class=\"selectMode\" *ngIf=\"(query.set &amp;&amp; query.set.html!='' &amp;&amp; query.set.part) &amp;&amp; query.results!=[]\"><div class=\"grid-100 bg-white padded-20 rounded-bottom\"><my-html *ngIf=\"query.set.html\" [url]=\"query.set.html\" [part]=\"query.set.part\" [reqAuth]=\"query.set.requireAuth\"></my-html></div></div><div class=\"result panel uk-card uk-card-default uk-box-shadow-medium rounded-bottom soloMode\"><div class=\"grid-100\" *ngIf=\"!isMobile\"><a class=\"uk-align-right closeBtn-embedded\" href=\"#\" (click)=\"closeQuery(query)\" *ngIf=\"query.set!=null\" role=\"button\" aria-label=\"Back to menu\" tabindex=\"0\"><i class=\"fa fa-chevron-left\"></i>Back</a></div><div class=\"grid-100 catBar\"><h1 style=\"font-size:26px;font-weight:300;width:100%;margin:0;padding:0;\" [innerHtml]=\"query.set.name\"></h1><h2 style=\"font-size:16px;;margin:0;padding:0;\" [innerHtml]=\"query.parent.set.name\" *ngIf=\"query.parent!=null\"></h2><div *ngIf=\"!isMobile &amp;&amp; service.currentQuery.set &amp;&amp; service.editMode==true\"><select id=\"catSelector\" [(ngModel)]=\"service.currentQuery.set.part\" style=\"width:100%;\"><option *ngFor=\"let h of service.headings\" [ngValue]=\"h.partID\">{{h.code}}: {{h.label}} :{{h.partID}}</option></select></div></div><div class=\"clear\"></div><div class=\"grid-100\" *ngIf=\"query.set.html &amp;&amp; query.set.part\"><hr/><my-html *ngIf=\"query.set.html\" [list]=\"CategoryListComponent\" [url]=\"query.set.html\" [part]=\"query.set.part\" [reqAuth]=\"query.set.requireAuth\" (partChange)=\"newHeading()\" (click)=\"htmlClick($event)\"></my-html></div></div></div><div class=\"uk-clearfix\"></div></div><div class=\"mobile-button-spacer hide-on-desktop\"></div><div class=\"uk-clearfix\"></div>"

/***/ }),

/***/ 407:
/***/ (function(module, exports) {

module.exports = "<nav class=\"topNav\" *ngIf=\"isMobile==true &amp;&amp; !showSplash &amp;&amp; queries[0].set!=null\" role=\"navigation\"><div class=\"uk-align-left closeBtn\" (click)=\"closeQuery(query)\" *ngIf=\"queries[0].set!=null\" tabindex=\"1\" role=\"button\"><i class=\"fa fa-chevron-left\"></i>Back</div></nav><main class=\"splash uk-container padding-side-0 scrollable\" [ngClass]=\"{'isMobile': isMobile}\"><a id=\"theTop\" aria-label=\"top\"></a><div class=\"grid-container padding-side-0\"><div class=\"grid-100\" [ngClass]=\"{'parentPad': showSplash &amp;&amp; isMobile}\"><!--============== SPLASH PAGE =================--><header class=\"splashPage\" *ngIf=\"(showSplash &amp;&amp; isMobile) &amp;&amp; isPortrait\" role=\"banner\"><div class=\"logo\" title=\"Off Campus logo\"></div><div class=\"blurb mobile overlay-1\"><div class=\"padded\"><!--h3.text-center(style=\"font-weight:100\") Monash University's--><p class=\"mobile-alt text-center\" style=\"font-size:120%;\"></p><p class=\"mobile-alt text-center\" style=\"font-size:90%;\"></p><br/><div class=\"splashBar\"><a (click)=\"showSplash=false;dimBg()\" aria-label=\"Start\" title=\"Start\" role=\"button\" href=\"#\"><i class=\"fa fa-chevron-down\"></i></a></div></div><br/><br/></div><br/><br/><div class=\"monash-logo\"></div></header><!--============== MAIN  =================--><div class=\"main\" *ngIf=\"!showSplash || !isMobile\"><header class=\"blurb\" *ngIf=\"isMobile!=true\" role=\"banner\"><h3 class=\"text-center\">Monash University's</h3><div class=\"logo\" title=\"OFF CAMPAS Limps logo\"></div><p class=\"padded mobile-alt text-center\"><a (click)=\"goEdit()\" *ngIf=\"service.currentQuery &amp;&amp; service.currentQuery.set &amp;&amp; service.editable\">[EDIT]</a><a (click)=\"loadLocal()\" *ngIf=\"service.currentQuery &amp;&amp; service.currentQuery.set &amp;&amp; service.editable\">[LOAD]</a><a (click)=\"saveLocal()\" *ngIf=\"service.currentQuery &amp;&amp; service.currentQuery.set &amp;&amp; service.editable\">[SAVE]</a></p></header><section class=\"white block-center\" id=\"opener\" *ngIf=\"!service.userValid()\"><h3 class=\"white text-center\" style=\"background-color:#000;height:38px;\">It's an Incident - what will you do straight away?</h3><p class=\"text-center\">The following response protocols are the actions you are required to implement during an incident.</p><div class=\"generic\"><div class=\"grid-100\"><div>O</div><div><p>Organise</p></div><div><p>Organise your immediate safety and team safety (eg First aid, 000)</p></div></div><div class=\"grid-100\"><div>F</div><div><p>Facts</p></div><div><p>Facts Collect immediate facts about the incident</p></div></div><div class=\"grid-100\"><div>F</div><div><p>Find your manager</p></div><div><p>Report to your event manager on what has happened as soon as you can</p></div><p>&nbsp;</p></div><div class=\"grid-100\"><div>C</div><div><p>Communicate</p></div><div><p>Communicate with your team on actions to take</p></div></div><div class=\"grid-100\"><div>A</div><div><p>Act</p></div><div><p>Act on instructions from emergency services and your senior Manager</p></div></div><div class=\"grid-100\"><div>M</div><div><p>Media</p></div><div><p>Do not speak with media, the public, external parties or via social media on the incident</p></div></div><div class=\"grid-100\"><div>P</div><div><p>Prepare</p></div><div><p>Prepare to relocate to an alternate location if needed</p></div></div><div class=\"grid-100\"><div>U</div><div><p>Understand</p></div><div><p>Understand how you and your team are feeling. Seek appropriate support</p></div></div><div class=\"grid-100\"><div>S</div><div><p>Summarise</p></div><div><p>Summarise the incident and complete the incident report</p></div></div></div><section class=\"grid-100 text-center\" id=\"splashNumbers\" aria-label=\"Emergency numbers\"><br/><p class=\"hide-on-desktop\">&nbsp;</p><h4 class=\"text-center white\">Emergency Numbers</h4><div class=\"category-item-square\"><div class=\"ci-block wideMenu\"><div class=\"ci-text\">Main Number <i class=\"fa fa-phone-square\" aria-hidden=\"true\"></i> <a class=\"telelink\" href=\"tel:9902 7777\">9902 7777</a></div></div></div></section><p class=\"hide-on-desktop\">&nbsp;</p><div class=\"grid-100 text-center\" *ngIf=\"service.userValid()==false\"><br/><p>You are required to login to access further information.</p><hr/><input [(ngModel)]=\"service.userPass\" aria-label=\"Enter password to view further content.\"/></div></section><div class=\"lists\" id=\"navMen\" *ngIf=\"service.userValid()==true\"><div class=\"aria\" role=\"heading\" aria-level=\"1\">Main menu</div><div class=\"no-pad-margin\" *ngFor=\"let q of queries\"><div *ngIf=\"(q.set || q.choose==true) &amp;&amp; q.show==true\" [ngClass]=\"{'category-item-square': q.id==0 &amp;&amp; q.set==null}\"><category-list class=\"grid-50 mobile-grid-100\" [query]=\"q\" (change)=\"handleUpdate($event)\" [ngClass]=\"{'solo': q.choose==false &amp;&amp; (q.set &amp;&amp; q.set.forceWide==true), 'mainMenu':q.id==0}\"></category-list></div></div></div></div><!--============== EDITOR PAGE =================--><div class=\"clearfix editPanel\" *ngIf=\"!isMobile &amp;&amp; (service.currentQuery &amp;&amp; service.currentQuery.set &amp;&amp; service.editMode==true)\" style=\"position: absolute;left: 0px;top: 50px;width: 410px;\"><div class=\"grid-100 bg-white\" style=\"padding:8px;\"><div class=\"grid-100\"><label>Name</label><input style=\"font-size:18px;font-weight:300;\" [(ngModel)]=\"service.currentQuery.set.name\"/><label>CSS Code</label><input style=\"font-size:18px;font-weight:300;\" [(ngModel)]=\"service.currentQuery.set.part\"/><label>HTML file</label><input style=\"font-size:18px;font-weight:300;\" [(ngModel)]=\"service.currentQuery.set.html\"/><br/><br/><label class=\"grid-30\">Parent</label><select [(ngModel)]=\"service.targetParent\" style=\"width:100%;\" (change)=\"service.moveCategory()\"><option *ngFor=\"let cl of service.categoriesList\" [ngValue]=\"cl\" [ngClass]=\"{'lv0': cl.level==0,'lv1': cl.level==1,'lv2': cl.level==2}\">{{spacer(cl.level)}}{{cl.name}}</option></select><label class=\"grid-30\">Icon path</label><input class=\"grid-70\" [(ngModel)]=\"service.currentQuery.set.icon\"/><label class=\"grid-30\">Wide Panel</label><input class=\"grid-10 suffix-60\" [(ngModel)]=\"service.currentQuery.set.forceWide\" type=\"checkbox\"/><label class=\"grid-30\">Major Incident</label><input class=\"grid-10 suffix-60\" [(ngModel)]=\"service.currentQuery.set.major\" type=\"checkbox\"/><label class=\"grid-30\">Two lines<input class=\"grid-10 suffix-60\" [(ngModel)]=\"service.currentQuery.set.twoLines\" type=\"checkbox\"/></label></div><div class=\"grid-100\"><br/><h5 class=\"grid-100\" style=\"margin:0;\">Subcategories</h5><ul style=\"list-style:none;padding:0;margin:0;\"><li class=\"grid-100\" *ngFor=\"let c of service.currentQuery.set.subcategories; let i=index\"><input class=\"grid-70\" [(ngModel)]=\"c.name\"/><div class=\"grid-30\"><a (click)=\"service.removeItem(i)\" style=\"margin-right:8px;\">Remove</a><a (click)=\"service.moveItem(service.currentQuery.set.subcategories,i,-1)\" style=\"margin-right:4px;\"><i class=\"fa fa-chevron-up\"></i></a><a (click)=\"service.moveItem(service.currentQuery.set.subcategories,i,1)\"><i class=\"fa fa-chevron-down\"></i></a></div></li></ul></div><hr/><a (click)=\"service.addItem()\">Add New Category</a><span>&nbsp;|&nbsp;</span><a (click)=\"service.dumpJson()\">Dump JSON</a><div *ngIf=\"service.currentQuery.set.part=='.l1.sub.otherNumbers'\"><div *ngFor=\"let num of service.appGlobals.numbers.other, let i=index\"><div class=\"grid-100\"><input class=\"grid-60\" [(ngModel)]=\"num.label\"/><input class=\"grid-60\" [(ngModel)]=\"num.ph\"/><a class=\"grid-10\" (click)=\"service.removeNumber(i)\">Remove</a></div><hr/></div><a (click)=\"service.addNumber()\">Add Phone Number</a><p>NOTE: You will need to navigate back then return to Other numbers to see your modifications.</p></div><textarea [ngModel]=\"categoryData\"></textarea></div></div><div class=\"clearfix\"></div><!--============== FOOTER PAGE =================--><div *ngIf=\"showSplash==false || isMobile==false\"><br class=\"hide-on-desktop\"/><br/><br/><div class=\"footer text-center\" *ngIf=\"queries[0] &amp;&amp; !queries[0].set || isMobile\" role=\"contentinfo\"><img src=\"assets/images/logo/monash.png\" alt=\"Monash University logo\"/><p>&copy; Monash University 2017</p></div></div><div class=\"clearfix\"></div></div></div></main>"

/***/ }),

/***/ 447:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 448:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 450:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(181);


/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(416);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__category__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_cheerio__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_cheerio___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_cheerio__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_underscore__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_underscore__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryDataService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CategoryDataService = (function () {
    function CategoryDataService(http) {
        this.http = http;
        this.dataUrl = "appData.json";
        this.HTMLUrl = "assets/html/sports.html";
        this.editable = false;
        this.editMode = false;
        this.emerg = {};
        this.appGlobals = window['appGlobals'];
        this.htmlPartData = '';
        this.htmlHeading = "";
        this.groupPass = "123";
        this.userPass = '';
        this.headings = [];
        this.dataHTML = '';
        this.test = "Test";
        this.popups = [
            { name: "NONE", id: 1 },
        ];
        this.icons = [
            { name: "doe-copyright-icon-apps.svg" },
        ];
        this._http = http;
        this.userNav = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        //this.loadFile(this.HTMLUrl);
    }
    CategoryDataService.prototype.setHTMLData = function (s) {
        console.log("setHTMLData");
        this.dataHTML = s;
    };
    CategoryDataService.prototype.userValid = function () {
        if (this.userPass == "1049")
            this.editable = true;
        return this.userPass == this.groupPass || this.userPass == "1049";
    };
    CategoryDataService.prototype.changeNav = function () {
        console.log('Change Nav Event..now emitting');
        this.userNav.emit({ test: 'ok' });
        console.log('emit done');
    };
    CategoryDataService.prototype.addNumber = function () {
        this.appGlobals.numbers.other.push({ "label": "New Number", "ph": "990123456", "contact": "First Last Name, Campus Security" });
    };
    CategoryDataService.prototype.removeNumber = function (idx) {
        this.appGlobals.numbers.other.splice(idx, 1);
    };
    CategoryDataService.prototype.removeItem = function (idx) {
        console.log('Remove Item');
        this.currentQuery.set.subcategories.splice(idx, 1);
    };
    CategoryDataService.prototype.moveItem = function (array, idx, offset) {
        var index = idx;
        var newIndex = index + offset;
        if (newIndex > -1 && newIndex < array.length) {
            var removedElement = array.splice(index, 1)[0];
            array.splice(newIndex, 0, removedElement);
        }
    };
    CategoryDataService.prototype.moveCategory = function (idx) {
        console.log('Move item');
        var item = this.currentQuery.set; //get current category
        this.targetParent.subcategories.push(item); //insert category
        this.currentQuery.parent.set.subcategories.splice(this.currentQuery.set['idx'], 1);
    };
    CategoryDataService.prototype.addItem = function () {
        console.log('Add Item');
        var c = new __WEBPACK_IMPORTED_MODULE_5__category__["a" /* Category */]();
        this.currentQuery.set.subcategories.push(c);
    };
    CategoryDataService.prototype.getData = function () {
        console.log("Service: getData");
        return this.http.get("./assets/appData.json")
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //.map(response => this.fileData = response.text())
    CategoryDataService.prototype.download = function (filename, text) {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    };
    CategoryDataService.prototype.dumpJson = function () {
        //dump JSon
        var o = {
            categories: this.categories,
            defaultHeaders: [
                "Something",
                "Something else",
                "Finally"
            ],
            useDefaults: true,
            pass: 123,
            numbers: this.appGlobals.numbers
        };
        this.download("appData.json", JSON.stringify(o));
    };
    CategoryDataService.prototype.buildCategories = function () {
        console.log('buildCategories');
        console.log(this.categories);
        var r = [];
        __WEBPACK_IMPORTED_MODULE_7_underscore__["each"](this.categories['subcategories'], function (e, i) {
            e.level = 0;
            r.push(e);
            __WEBPACK_IMPORTED_MODULE_7_underscore__["each"](e.subcategories, function (e1, i1) {
                e1.level = 1;
                r.push(e1);
                __WEBPACK_IMPORTED_MODULE_7_underscore__["each"](e1.subcategories, function (e2, i2) {
                    e2.level = 2;
                    r.push(e2);
                });
            });
        });
        this.categoriesList = r;
    };
    CategoryDataService.prototype.buildHeadings = function () {
        var _this = this;
        console.log('buildHeadings');
        this.getFile(this.currentQuery.set.html).subscribe(function (res) {
            var $ = __WEBPACK_IMPORTED_MODULE_6_cheerio__["load"](res);
            var r = $('.heading');
            var a = [];
            r.each(function (i, elem) {
                var c = $(elem).parent().attr('class');
                a.push({ label: $(elem).text(), partID: '.' + c.split(' ').join('.'), code: $(elem).parent().find('.code').text() });
            });
            _this.headings = a;
            _this.buildCategories();
        });
    };
    CategoryDataService.prototype.getFile = function (f) {
        var _this = this;
        return this._http
            .get(f)
            .map(function (response) { return _this.fileData = response.text(); });
    };
    CategoryDataService.prototype.validateUser = function () {
        var v = (this.userPass == this.groupPass) ? true : false;
        console.log(v);
        return v;
    };
    CategoryDataService.prototype.findCategory = function (s) {
        console.log('Service: findCategory ' + s);
        var cat = this.customFilter(this.categories, s);
        return cat;
    };
    CategoryDataService.prototype.customFilter = function (object, s) {
        if (object.hasOwnProperty('part') && object["part"] === s)
            return object;
        for (var i = 0; i < Object.keys(object).length; i++) {
            if (typeof object[Object.keys(object)[i]] == "object") {
                var o = this.customFilter(object[Object.keys(object)[i]], s);
                if (o != null)
                    return o;
            }
        }
        return null;
    };
    CategoryDataService.prototype.handleError = function (error) {
        // In a real world app, you might use a remote logging infrastructure
        var errMsg;
        console.log("ERROR");
        console.error(error);
        if (error instanceof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Response */]) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(errMsg);
    };
    return CategoryDataService;
}());
CategoryDataService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object])
], CategoryDataService);

var _a;
//# sourceMappingURL=category-data.service.js.map

/***/ })

},[450]);
//# sourceMappingURL=main.bundle.js.map